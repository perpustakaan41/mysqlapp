# -*- coding: utf-8 -*-
"""
Created on Mon Aug 30 13:56:33 2021

@author: ROG
"""

from flask.json import jsonify
from flask import request
from models.customers import database
from flask import Flask
import json

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False

@app.route("/")
def main():
    return "Welcome!"

@app.route("/user", methods=["GET", "POST"])
def showUser():
    try:
        if request.method == "GET":
            dbresult = db.showUsers()
            result = []
            for item in dbresult:
                user = {
                    "id": item[0],
                    "username": item[1],
                    "firstname": item[2],
                    "lastname": item[3],
                    "email": item[4]
                    }
                result.append(user)
            return jsonify(result)
        else:
            try:
                params = request.json
                dbresult = db.showUserById(**params)
                user = {
                    "id": dbresult[0],
                    "username": dbresult[1],
                    "firstname": dbresult[2],
                    "lastname": dbresult[3],
                    "email": dbresult[4]
                    }
                return jsonify(user)
            except Exception as e:
                message = {
                    "msg": "id yang diminta tidak ada"
                }
                return jsonify(message)

    except Exception as e:
        print(f"kesalahan routing{e}")

if __name__ == "__main__":
    db = database()
    if db.db.is_connected():
        print("connected to mysql")
    
    app.run(port=5000, debug=True)

    if db.db is not None and db.db.is_connected():
        db.db.close()