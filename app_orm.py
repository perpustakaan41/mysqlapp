from models.customers_orm import Database

from flask import Flask, request
import json
from flask.json import jsonify

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False

@app.route("/")
def get():
    return "Aplikasi ini sudah berhasil dijalankan"

@app.route("/user", methods=["GET", "POST"])
def user():
    try:
        if request.method == "POST":
            try:
                params = request.json
                dataUser = db.showUserById(**params)
                data = {
                    "userid": dataUser.USERID,
                    "username": dataUser.USERNAME,
                    "namadepan": dataUser.NAMADEPAN,
                    "namabelakang": dataUser.NAMABELAKANG,
                    "email": dataUser.EMAIL
                }
                return jsonify(data)
            except Exception as e:
                msg = {
                    "msg": e
                }
                return jsonify(msg)
        else:
            try:
                dataUser = db.showUser()
                hasil = []
                for item in dataUser:
                    data = {
                        "userid": item.USERID,
                        "username": item.USERNAME,
                        "namadepan": item.NAMADEPAN,
                        "namabelakang": item.NAMABELAKANG,
                        "email": item.EMAIL
                        }
                    hasil.append(data)
                return jsonify(hasil)
            except Exception as e:
                msg = {
                    "msg": e
                }
                return jsonify(msg)
    except Exception as e:
        msg={
            "msg": "kesalahan",
            "keterangan": e
        }
        return jsonify(msg)

if __name__ == "__main__":
    db = Database()
    app.run(port=5151, debug=True)

