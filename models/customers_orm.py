from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, String, Integer
Base = declarative_base()

class Customers(Base):
    __tablename__ = "customers"
    USERID = Column(Integer, primary_key=True)
    USERNAME = Column(String)
    NAMADEPAN = Column(String)
    NAMABELAKANG = Column(String)
    EMAIL = Column(String)


class Database:
    def __init__(self):
        try:
            self.engine = create_engine(
                "mysql+mysqlconnector://root:@localhost:3306/perpustakaan",
                echo=True
            )
            self.Session = sessionmaker(bind=self.engine)
            self.session = self.Session()
        except Exception as e:
            print(f"kesalahan koneksi class Database: {e}")

    def showUser(self):
        try:
            dataUser = self.session.query(Customers).all()
            return dataUser
        except Exception as e:
            print(f"kesalahan function showUser: {e}")

    def showUserById(self, **params):
        try:
            id_user = params["userid"]
            dataUser = self.session.query(Customers).filter(Customers.USERID == id_user).one()
            return dataUser
        except Exception as e:
            print(f"kesalahan function showUserById: {e}")